#coding=utf-8
# Create your views here.
from django.http import HttpResponse
from django.shortcuts import render_to_response, RequestContext, render
from django.views.decorators.csrf import csrf_protect, csrf_exempt

import markdown
import os

def planup(request):
    FILE_PATH = os.path.join(os.path.dirname(__file__), '..', 'files').replace('\\','/')
    mdFilePath = str(FILE_PATH) + "/plan.md"

    STATIC_PATH = os.path.join(os.path.dirname(__file__), '../static').replace('\\', '/')
    cssFilePath = str(STATIC_PATH) + "/css/GitHub2.css"

    # input = "## cleantha \n * cleantha"
    '''mdInput = open(mdFilePath)
    text = mdInput.read()
    print text
    html = markdown.markdown(text.encode('gbk'))
    print html
    return HttpResponse(html)'''

    # text = '# 大标题 ## 第二号 ... '
    '''css = open(cssFilePath)
    csstext = css.read()'''
    csstext = '''<link rel="stylesheet" type="text/css" href="/site_media/css/GitHub2.css"/>'''
    cssmd = unicode(csstext, 'utf-8')
    mdInput = open(mdFilePath)
    text = mdInput.read()
    md = unicode(text, 'utf-8')
    finalmd = cssmd + '\n' + md
    result = markdown.markdown(finalmd).encode('utf-8')
    print result
    return HttpResponse(result)

    # return render_to_response('plan.html', {})

def index(request):
    return render_to_response('index.html', {})

from django.utils.translation import ugettext

def my_view(request):
    output = ugettext("Welcome to my site.")
    return HttpResponse(output)

@csrf_protect
# @csrf_exempt
def postpost(request):
    if request.method == 'GET':
        return render_to_response('postpost.html', {}, context_instance=RequestContext(request))
        # return render_to_response('postpost.html', {})
    if request.method == 'POST':
        print request.POST['clea']
        return render_to_response('afterpost.html', {})
        # return render_to_response('afterpost.html', {}, context_instance=RequestContext(request))

def inner(request):
    return render_to_response('inner.html', {})

def haha(request):
    # return render_to_response('index.html', {})
    return render(request, 'index.html', {'post_list': None,
            'user': None,'category':None, 'contacts': None})