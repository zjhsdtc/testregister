#_*_coding:utf-8_*_

from django.conf.urls import patterns, include, url

from register import views

urlpatterns = patterns('',
    
    url(r'^markdown/$', views.planup),
    url(r'^index/$', views.index, name="index"),
    url(r'^trans/$', views.my_view),
    url(r'^postpost/$', views.postpost),
    url(r'^inner/$', views.inner),
    # url(r'^haha/$', views.haha),
    url(r'^haha/$', 'register.views.haha')
)
