from django.conf.urls import patterns, include, url

# Uncomment the next two lines to enable the admin:
# from django.contrib import admin
# admin.autodiscover()

from register import views
from testregister import settings

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'testregister.views.home', name='home'),
    # url(r'^testregister/', include('testregister.foo.urls')),

    # Uncomment the admin/doc line below to enable admin documentation:
    # url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
    # url(r'^admin/', include(admin.site.urls)),
    url(r'^site_media/(?P<path>.*)$', 'django.views.static.serve', {'document_root': settings.STATIC_PATH}),
    (r'^accounts/', include('registration.backends.default.urls')),
)

urlpatterns += patterns('',
    url(r'', include('register.urls')),
)
